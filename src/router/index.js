import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/culture",
    name: "Culture",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Culture.vue"),
  },
  {
    path: "/logo-identitas",
    name: "Logo-Identitas",
    component: () =>
      import(/* webpackChunkName: "Logo-Identitas" */ "../views/Logo.vue"),
  },
  {
    path: "/website",
    name: "Website",
    component: () =>
      import(/* webpackChunkName: "Website */ "../views/Website.vue"),
  },
  {
    path: "/video-company-profile",
    name: "Video",
    component: () =>
      import(/* webpackChunkName: "Website */ "../views/Video.vue"),
  },
  {
    path: "/order",
    name: "Order",
    component: () =>
      import(/* webpackChunkName: "Website */ "../views/Order.vue"),
  },
  {
    path: "/preview",
    name: "Preview",
    component: () =>
      import(/* webpackChunkName: "Website */ "../views/Preview.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
