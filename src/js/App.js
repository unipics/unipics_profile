export default {
  name: "app",
  data: () => ({
    navbarShadow: false,
  }),
  methods: {
    navbarMobile: function() {
      var x = document.getElementById("myTopnav");
      const sticky = x.offsetTop;

      if (window.pageYOffset > sticky) {
        if (x.className === "topnav shadow-header") {
          x.className += " responsive";
        } else {
          x.className = "topnav shadow-header";
        }
      } else {
        if (x.className === "topnav") {
          x.className += " responsive shadow-header";
        } else {
          x.className = "topnav";
        }
      }
    },

    isPageScrolled: function() {
      const headerPosition = document.getElementById("myTopnav");
      const sticky = headerPosition.offsetTop;
      if (window.pageYOffset > sticky) {
        this.navbarShadow = true;
      } else {
        this.navbarShadow = false;
      }
    },
    onNavbarClicked: function() {
      document.getElementById("close").click();
      window.scroll(0, 0);
    },
    gotoHome: function() {
      // window.scroll(0, 0);
      // this.$router.push({ path: "/" });
      // location.reload();
      var x = document.getElementById("myTopnav");
      if (x.className === "topnav responsive") {
        window.scroll(0, 0);
        document.getElementById("close").click();
      } else {
        window.scroll(0, 0);
      }
    },
  },
  components: {},
  mounted() {
    window.onscroll = this.isPageScrolled;
  },
  watch: {},
};
